<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201001174750 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create Log table';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('log');
        $table->addColumn('id', 'integer')->setAutoincrement(true);
        $table->addColumn('url', 'string');
        $table->addColumn('request', 'text');
        $table->addColumn('response', 'text');
        $table->addColumn('status', 'integer');
        $table->addColumn('ip', 'string')
            ->setNotnull(false)
            ->setLength(15)
        ;
        $table->addColumn('created_at', 'datetime');
        $table->addIndex(['id'], 'idx_id');
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('log');
    }
}
