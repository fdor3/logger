<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Log;
use App\Form\IpFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(Request $request): Response
    {
        return new Response();
    }

    public function log(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(IpFilterType::class);
        $form->handleRequest($request);

        $ip = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $ip = $form->getData()->getIp();
        }

        $logRepository = $em->getRepository(Log::class);

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'logRows' => $logRepository->findBy($ip !== null ? ['ip' => $ip] : []),
        ]);
    }
}
