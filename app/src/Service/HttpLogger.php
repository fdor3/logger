<?php

namespace App\Service;

use App\Service\Handler\HandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HttpLogger
{
    /** @var HandlerInterface  */
    private $handler;

    public function __construct(HandlerInterface $handler)
    {
        $this->handler = $handler;
    }

    public function log(Request $request, Response $response): void
    {
        if ($request->headers->get('Logger') === 'on') {
            $this->handler->log($request, $response);
        }
    }
}
