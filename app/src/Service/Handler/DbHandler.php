<?php

namespace App\Service\Handler;

use App\Entity\Log;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DbHandler implements HandlerInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function log(Request $request, Response $response): void
    {
        $log = new Log();
        $log->setUrl($request->getRequestUri());
        $log->setRequest($request->headers . '; ' . $request->getContent());
        $log->setResponse($response->headers . '; ' . $response->getContent());
        $log->setStatus($response->getStatusCode());
        $log->setIp($request->getClientIp());
        $log->setCreatedAt(new \DateTime());

        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }
}
