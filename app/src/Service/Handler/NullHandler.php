<?php

namespace App\Service\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NullHandler implements HandlerInterface
{
    public function __construct(ContainerInterface $container)
    {

    }

    public function log(Request $request, Response $response): void
    {

    }
}
