<?php

namespace App\Service\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface HandlerInterface
{
    public function log(Request $request, Response $response): void;
}
