<?php

namespace App\EventSubscriber;

use App\Service\HttpLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class HttpSubscriber implements EventSubscriberInterface
{
    /** @var HttpLogger */
    private $logger;

    public function __construct(HttpLogger $logger)
    {
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => [
                ['onResponse', 0],
            ],
        ];
    }

    public function onResponse(ResponseEvent $event)
    {
        $this->logger->log($event->getRequest(), $event->getResponse());
    }
}
